<?php

namespace App\Package\ErrorLog\src;

use Illuminate\Support\ServiceProvider;

class ErrorLogServiceProvider extends ServiceProvider {
    public function register() {
    }

    public function boot() {
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'Errorlog');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');

        $this->publishes([
            __DIR__.'/public' => public_path(''),
        ], 'public');
    }
}
