<?php

namespace App\Package\ErrorLog\src\models;

use App\common\Common;
use Carbon\Carbon;
use App\Package\ErrorLog\src\controllers\Base\ErrorLogBaseController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MasterModel extends Model
{
    // get master data
    public function getMasterDetails($table_name)
    {
        $data = array();
        $data = DB::table($table_name)->paginate(5);
        return $data;
    }


    /**
     * master_insert method
     *
     * @access	public
     * @param	array data
     * @param	string tablename
     * @return	int last inserted id
     */
 public function insertData($request, $table_name)
    {
        try {
            $sessionData = session()->get('user_info');
            if(isset($sessionData) && !empty($sessionData)){
                $sessionData = session()->get('user_info');
                $created_by  = $sessionData['user_id'];
            }
            else{
                $created_by = '0';
            }

            $currentDate = Carbon::now();// returns the current date and time
            $request['created_by'] =  !empty($request['created_by']) ? $request['created_by'] :$created_by ;
            $request['updated_by'] =  !empty($request['updated_by']) ? $request['updated_by'] : $created_by;
            $request['created_at'] = $currentDate->toDateTimeString();
            $request['updated_at'] = $currentDate->toDateTimeString();
            //$request['is_deleted'] = 0;
            //$request['is_completed'] = 0;
            //echo "<pre>";print_r($request);die;
            $rows_affected = DB::table($table_name)->insert($request);

            return DB::getPdo()->lastInsertId();
        }catch(\Exception $ex){

            $ErrorlogBaseController = new ErrorLogBaseController();
            $ErrorlogBaseController->error_logging($ex,'insertData', 'MasterModel.php');
            return view('layouts.coming_soon');
        }
    }

    public static function queryBinder($extraSettings = [], $query)
    {
        try {
            if (!empty($extraSettings['where']) && count($extraSettings['where']) > 0) {
                foreach ($extraSettings['where'] as $whereListKey => $whereList) {
                    if ($whereListKey == 'orWhere') {
                        $query->where(function ($q) use ($whereList) {
                            foreach ($whereList as $whereKey => $whereDetail) {
                                $q->OrWhere($whereDetail['column'], $whereDetail['expression'], $whereDetail['value']);
                            }
                        });
                    } else if ($whereListKey == 'where') {
                        foreach ($whereList as $whereKey => $whereDetail) {
                            $query->where($whereDetail['column'], $whereDetail['expression'], $whereDetail['value']);
                        }
                    } else if ($whereListKey == 'whereIn') {
                        foreach ($whereList as $whereKey => $whereDetail) {
                            $query->whereIn($whereDetail['column'], $whereDetail['value']);
                        }
                    } else if ($whereListKey == 'whereNotIn') {
                        foreach ($whereList as $whereKey => $whereDetail) {
                            $query->whereNotIn($whereDetail['column'], $whereDetail['value']);
                        }
                    }
                }
            }
            return $query;
        } catch (\Exception $ex) {
            $ErrorlogBaseController = new ErrorLogBaseController();
            $ErrorlogBaseController->error_logging($ex,'queryBinder', 'MasterModel.php');
            return view('layouts.coming_soon');
        }
    }


}
