<?php

namespace App\Package\ErrorLog\src\models;

use Illuminate\Foundation\Auth\User as Authenticatable;
class ErrorlogModel extends Authenticatable
{
	 //Use Updater;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['date', 'time', 'user_id', 'user_name', 'page', 'function', 'error_code', 'description', 'created_at', 'updated_at', 'created_by', 'updated_by'];
    public $table       = 'error_logs';
}
